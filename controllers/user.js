let posts = require('../db/users.json');

module.exports = {
    index: function (req, res) {
        res.render('user/register');
        res.status(200);
    },
    store: function (req, res) {
        const email = req.body.email;
        const password = req.body.password;
        if (email && password) {
            const newUser = {
                email: email,
                password: password
            };
            posts.push(newUser);
            res.render('games/game');
            res.status(200);
        } else {
            res.send('Please enter Username and Password!');
        }
    },
    view: (req, res) => {
        res.status(200).json({ posts })
    }

}