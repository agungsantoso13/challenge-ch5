const express = require('express');
const app = express();
const morgan = require('morgan');
const port = 3000;

const userRouter = require('./router/user');

//Middleware
app.use(express.json());
app.use(express.urlencoded({ extended: false }));
app.use(morgan('dev'));

app.set('view engine', 'ejs');
app.use(express.static('public'));


// End point untuk main call to action(landing page)
app.get('/', (req, res) => {
    res.render('pages/landing');
    res.status(200);
});

//2
app.get('/about', (req, res) => {
    res.render('pages/about');
    res.status(200);
});

//3
app.get('/feature', (req, res) => {
    res.render('pages/feature');
    res.status(200);
});

//4
app.get('/require', (req, res) => {
    res.render('pages/require');
    res.status(200);
});

//5
app.get('/quote', (req, res) => {
    res.render('pages/quote');
    res.status(200);
});

//End point user&login
app.use(userRouter);

//listening
app.listen(port, () => {
    console.log(`Server Nyala! di ${port}`)
});