const express = require('express');
const router = express.Router();
const userController = require('../controllers/user');

//End point register
router.route('/user')
    .get(userController.index)
    .post(userController.store)

//End point Data User
router.get('/user/data', userController.view)

module.exports = router;