
// JS untuk game BatuGuntingKertas

// pilihan computer
const listPilihan = ['batu', 'gunting', 'kertas'];

function getPilihanKomputer() {
    const randomIdx = Math.floor((Math.random() * listPilihan.length));
    return komp = listPilihan[randomIdx];
}

//tambah highlight kotak img Komputer
function kompKotak() {
    if (komp == 'batu') {
        const kompBatu = document.querySelector('.area-komputer > ul:nth-child(2) > li:nth-child(1) > img');
        kompBatu.style.borderRadius = '10px';
        kompBatu.style.backgroundColor = 'gray';
        kompBatu.style.padding = '5px';

    } else if (komp == 'gunting') {
        const kompGunting = document.querySelector('.area-komputer > ul:nth-child(2) > li:nth-child(2) > img');
        kompGunting.style.borderRadius = '10px';
        kompGunting.style.backgroundColor = 'gray';
        kompGunting.style.padding = '5px';

    } else {
        const kompKertas = document.querySelector('.area-komputer > ul:nth-child(2) > li:nth-child(3) > img');
        kompKertas.style.borderRadius = '10px';
        kompKertas.style.backgroundColor = 'gray';
        kompKertas.style.padding = '5px';
    }
};

//hasil pilihan player dan komputer

function getHasil(komp, player) {

    if (player == komp) return 'DRAW';
    if (player == 'batu') return (komp == 'gunting') ? 'Player1 MENANG' : 'COM MENANG';
    if (player == 'gunting') return (komp == 'batu') ? 'COM MENANG' : 'Player1 MENANG';
    if (player == 'kertas') return (komp == 'gunting') ? 'COM MENANG' : 'Player1 MENANG';

};

const pilihan = document.querySelectorAll('.area-player > ul > li img');
pilihan.forEach(function (pil) {
    pil.addEventListener('click', function () {
        const pilihanKomputer = getPilihanKomputer();
        const pilihanPlayer = pil.className;
        const hasil = getHasil(pilihanKomputer, pilihanPlayer);


        pil.style.borderRadius = '10px';
        pil.style.backgroundColor = 'gray';
        pil.style.padding = '5px';

        kompKotak();

        const info = document.querySelector('.info > div');
        const infoKotak = document.querySelector('.info');
        info.setAttribute('class', 'kotak');
        info.innerHTML = hasil;
        infoKotak.style.backgroundColor = 'green';
        infoKotak.style.borderRadius = '20px';
        infoKotak.style.transform = 'rotate(-15deg)';
    });
});